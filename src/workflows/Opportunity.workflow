<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Contrac_Term_from_of_Months</fullName>
        <field>ACTNSCRM__Contract_Term__c</field>
        <formula>Pay_Periods__c</formula>
        <name>Set Contrac Term from # of Months</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contract_start_date_to_Sub_Start_Dat</fullName>
        <field>ACTNSCRM__Contract_Start_Date__c</field>
        <formula>Subscription_Start_Date__c</formula>
        <name>Set Contract start date to Sub Start Dat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Closed_Won</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Pantheon_Closed_Won_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change Opportunity Record Type to Won</fullName>
        <actions>
            <name>Set_Record_Type_to_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Contract Start Date</fullName>
        <actions>
            <name>Set_Contract_start_date_to_Sub_Start_Dat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Subscription_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>contract start date is equal to # of months filled in by Sales Rep.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Default Contract Term</fullName>
        <actions>
            <name>Set_Contrac_Term_from_of_Months</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Pay_Periods__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>contract term is equal to # of months filled in by Sales Rep.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
